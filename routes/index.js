const axios = require('axios');
const mongoose = require('mongoose');

const Sessions = require('../models/sessions');
const config = require('../config');

const handleConnection = _ => {
    return new Promise(((resolve, reject) => {
        mongoose.Promise = global.Promise;
        mongoose.connect(config.db.uri, {useNewUrlParser: true});
        const db = mongoose.connection;

        db.on('error', (err) => {
            reject('Could not connect to mongo')
        });

        db.once('open', () => {
            resolve(true)
        });
    }))

};
const getSessions = (req, res, next) => {
    if(req.params.id) {
        return Sessions.findById(req.params.id)
            .then(session => {
                const movieId = session.movie;
                return axios.get(`${config.services.movieDatabase}/movies?id=${movieId}`)
                    .then(result => {
                        session._doc.movie = result.data;
                        res.send(200, session);
                        return true
                    })
                    .catch(err => {
                        console.log(err);
                        return false
                    })

            })
            .catch(err => {
                console.error(err)
                return false
            })
    } else {
        const query = {};
        if(req.params.cinema){
            query.cinema = { $regex: '.*' + req.params.cinema + '.*', $options: 'i'}
        }
        return Sessions.find(query)
            .then(sessions => {
                res.send(200, sessions)
                return true
            })
            .catch(err => {
                console.error(err);
                return false
            })
    }

};
const postSessions = (req, res, next) => {
    const session = {
        state: req.body.state,
        cinema: req.body.cinema,
        datetime: req.body.datetime,
        spots: req.body.spots,
        movie: req.body.movie
    }
    const sessionObj = new Sessions(session);
    return sessionObj.save()
        .then(result => {
            console.log(result);
            res.send(201);
            return true;
        })
        .catch(err => {
            console.error(err);
            return false
        })

}
module.exports = function(server) {
    server.get('/', (req, res, next) => {
        res.send(200, "Booking is running")
    })
    server.get('/sessions', (req, res, next) => {
        handleConnection()
            .then(status => {
                return getSessions(req, res, next);
            })
            .then(res => {
                mongoose.disconnect()
            })
            .catch(err => {
                console.log(err);
                mongoose.disconnect()
            })
    });
    server.post('/sessions', (req, res, next) => {
        handleConnection()
            .then(status => {
                return postSessions(req, res, nex)
            })
            .then(res => {
                mongoose.disconnect()
            })
            .catch(err => {
                console.log(err)
                mongoose.disconnect()
            })
    })
};