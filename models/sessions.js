const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');


const SessionsSchema = new mongoose.Schema(
    {
        "state": {
            "type": "String"
        },
        "cinema": {
            "type": "String"
        },
        "datetime": {
            "type": "String"
        },
        "spots": {
            "type": "Number"
        },
        "movie": {
            "type": "String"
        },
    }

);

SessionsSchema.plugin(timestamps);

const SessionsModel= mongoose.model('sessions', SessionsSchema, 'sessions');
module.exports = SessionsModel;